const UserAgentParser = (function() {
	const Browscap = require("browscap");
	const browscap = new Browscap(browser.extension.getURL("deps/browscap-json-cache-files/build-0/sources"));
	
	class UserAgentParser {
		constructor(browser, userAgent) {
			this.browser = browser;
			this.userAgent = userAgent;
		}
		
		
		static parse(userAgent) {
			return Promise.resolve(browscap.getBrowser(userAgent)).then((browser) => {
				return new UserAgentParser(browser, userAgent);
			});
		}
		
		
		/**
		 * Try to make up a sensible/convincing `navigator.platform` string
		 *
		 * Based on: https://stackoverflow.com/a/19883965/277882
		 */
		get platform() {
			// Note that any CPU architectures (if provided) try to reflect the most common
			// results on the given platform – there are cases where they may be inaccurate
			// (i.e. an x86 Android Tablet reporting as "armv7l")
			if(this.browser["Win32"] || this.browser["Win64"]) {
				return "Win32";
			} else if(this.browser["Platform"] === "Linux") {
				return "Linux " + (this.browser["Browser_Bits"] != 64 ? "i686" : "x86_64");
			} else if(this.browser["Platform"] === "macOS" || this.browser["Platform"] === "MacOSX") {
				return "MacIntel";
			} else if(this.browser["Platform"] == "Android") {
				return "Linux " + (this.browser["Browser_Bits"] != 64 ? "armv7l" : "aarch64");
			} else if(this.browser["Platform"] === "iOS") {
				return (this.browser["isTablet"] ? "iPad" : (this.browser["isMobileDevice"] ? "iPhone" : "iPod"));
			} else if(this.browser["Platform"] === "FreeBSD") {
				return "FreeBSD " + (this.browser["Browser_Bits"] != 64 ? "i386" : "amd64");
			} else if(this.browser["Platform"] === "OpenBSD") {
				return "OpenBSD " + (this.browser["Browser_Bits"] != 64 ? "i386" : "amd64");
			} else {
				return this.browser["Platform"];
			}
		}
		
		
		/**
		 * Try to make up a sensible/convincing `navigator.oscpu` string
		 *
		 * Based on: https://developer.mozilla.org/en-US/docs/Web/API/Navigator/oscpu
		 */
		get oscpu() {
			let oscpu = this.browser["Platform"];
			if(this.browser["Win32"] || this.browser["Win64"]) {
				oscpu = "Windows NT";
			} else if(this.browser["Platform"] === "macOS" || this.browser["Platform"] === "MacOSX") {
				oscpu = "Intel Mac OS X";
			}
			
			if(this.browser["Platform_Version"]) {
				oscpu += " " + this.browser["Platform_Version"];
			}
			
			if(this.browser["Win64"]) {
				oscpu += "; " + (this.browser["Browser_Bits"] == 32 ? "WOW64" : "Win64; x64");
			} else if(this.browser["Platform"] === "Linux") {
				oscpu += " " + (this.browser["Browser_Bits"] != 64 ? "i686" : "x86_64");
			}
			
			return oscpu;
		}
	}
	
	return UserAgentParser;
})();